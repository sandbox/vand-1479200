
Breadcrumb Trimmer
------------------

Summary
-------
This module is built for Drupal 7 and is used to dynamically trim your sites
breadcrumbs to some maximum length, defined by you on the settings page.

The logic behind this module works like this:
* Get total length for breadcrumbs as defined by user
* Get total number of breadcrumbs for that node
* Set allowed space for each breadcrumb and allocate that space dynamically.
  This allows for the maximum amount of characters to be used, but never go over
  the limit that was set by the user.

Settings
--------
* On the settings page, define the max length for your breadcrumbs
* On the settings page, define the amount of characters your breadcrumb spacing
  takes up.

Author
------
Craig Vanderlinden
    Drupal.org - VanD
    Twitter - cvanderlinden
    Website - cvanderlinden.com